# This file is maintained automatically by "terraform init".
# Manual edits may be lost in future updates.

provider "registry.terraform.io/hashicorp/local" {
  version = "2.4.1"
  hashes = [
    "h1:FzraUapGrJoH3ZOWiUT2m6QpZAD+HmU+JmqZgM4/o2Y=",
  ]
}

provider "registry.terraform.io/yandex-cloud/yandex" {
  version = "0.110.0"
  hashes = [
    "h1:BUo/KwlI6jRUSMsO3e1ktXnAS6KJWxoaicPNFmNyLjM=",
  ]
}
