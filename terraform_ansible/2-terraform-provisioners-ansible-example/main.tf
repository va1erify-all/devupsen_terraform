terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = " >= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

resource "yandex_compute_instance" "vm1" {
  name        = "linux-vm1"
  platform_id = "standard-v2"

  scheduling_policy {
    preemptible = true
  }
  resources {
    cores         = var.cpu
    memory        = var.ram
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id   = "fd8t849k1aoosejtcicj"
      size       = 5
      type       = "network-hdd"
      block_size = 4096
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "${var.name_user}:${var.ssh_key}"
  }

}

resource "yandex_vpc_network" "network1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["10.10.10.0/24"]
}
/*

*/

# если запустить манифест повторно, при уже созданном local_file, то он скажет что изменений не было, потому что он хранится в tfstate. Пока результат выполнения не будет иным, terraform не будет перегенерировать его.
# на выходе получаем готовый inventory file
# ресурс "local_file" позволяет работать с ресурсами локально, однако это не провижн и мы можем обращаться к его атрибутам и использовать их в дальнейшем
resource "local_file" "ansible_inventory" {
  # content - описываем то, что будет в нашем файле
  # templatefile - на вход принимает путь до шаблона и переменные, которые, используются в этом шаблоне.
  content = templatefile("${path.module}/inventory.tpl",
    {
      host    = yandex_compute_instance.vm1.name
      address = yandex_compute_instance.vm1.network_interface.0.nat_ip_address
      app = "example"
    })
  # filename - куда будет генерировать наш шаблон
  filename = "${path.module}/inventory"
}

# мы создали файл с хостами, но ansible работает в другой директории. И на помощь нам может прийти local_exec - просто отправив инвентарь в нужное нам место
# Возможна такая реализация:
# 1) пул репозитория
# 2) создание ветки
# 3) обновление файла "inventory" на новый
# 4) commit
# 5) push
# Также можно например хранить файл с инвентарем в S3 хранилище и положить его туда curlом
