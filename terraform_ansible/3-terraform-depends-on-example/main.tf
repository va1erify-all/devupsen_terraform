terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = " >= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = var.zone
}

resource "yandex_compute_instance" "vm1" {
  name        = "linux-vm1"
  platform_id = "standard-v2"

  scheduling_policy {
    preemptible = true
  }
  resources {
    cores         = var.cpu
    memory        = var.ram
    core_fraction = 5
  }

  boot_disk {
    initialize_params {
      image_id   = "fd8t849k1aoosejtcicj"
      size       = 5
      type       = "network-hdd"
      block_size = 4096
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    ssh-keys = "${var.name_user}:${var.ssh_key}"
  }

  # указание зависимости, пока нет subnet, terraform не приступит к созданию ВМ.
  depends_on = [
    yandex_vpc_subnet.subnet-1
  ]

}

resource "yandex_vpc_network" "network1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["10.10.10.0/24"]

  depends_on = [
    yandex_vpc_network.network1
  ]
}