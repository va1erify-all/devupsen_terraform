variable "folder_id" {}
variable "cloud_id" {}
variable "token" {}

variable "zone" {
  default = "ru-central1-a"
}

variable "name_user" {}
variable "ssh_key" {}
variable "ram" {}
variable "cpu" {}