terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = " >= 0.13"
}

provider "yandex" {
  token     = var.token
  cloud_id  = var.cloud_id
  folder_id = var.folder_id
  zone      = "ru-central1-a"
}

resource "yandex_compute_instance" "vm1" {
  name        = "linux-vm"
  platform_id = "standard-v2"

scheduling_policy {
  preemptible = true
}
  resources {
    cores  = var.cpu
    memory = var.ram
    core_fraction = var.core_fraction
  }

  boot_disk {
    initialize_params {
      image_id   = var.boot_disk_image_id
      size       = var.boot_disk_size
      type       = var.boot_disk_type
      block_size = var.boot_disk_block_size
    }
  }

  network_interface {
    subnet_id = yandex_vpc_subnet.subnet-1.id
    nat       = true
  }

  metadata = {
    # cloud-config - инструмент, в котором можно описать минимальную конфигурацию сервера (что мы хотим там видить, каких пользователей и т.д)
    # там не нужно хранить то, что можно сделать через ansible, он используется для того чтобы машина пришла к нам в виде, в котором с ней уже удобно работать через ansible.
    # file - модуль terraform
    # path.module - путь до директории, в которой мы работаем.
    user-data = file("${path.module}/cloud-config")
  }
}

resource "yandex_vpc_network" "network1" {
  name = "network1"
}

resource "yandex_vpc_subnet" "subnet-1" {
  name           = "subnet1"
  network_id     = yandex_vpc_network.network1.id
  v4_cidr_blocks = ["10.10.10.0/24"]
}