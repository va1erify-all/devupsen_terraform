variable "folder_id" {}
variable "cloud_id" {}
variable "token" {}

variable "name_user" {}
variable "core_fraction" {}
variable "ram" {}
variable "cpu" {}

variable "boot_disk_image_id" {}
variable "boot_disk_size" {}
variable "boot_disk_type" {}
variable "boot_disk_block_size" {}