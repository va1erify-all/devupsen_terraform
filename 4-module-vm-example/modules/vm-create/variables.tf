variable "name" {
  description = "Name VM"
}

variable "platform" {
  default = "standard-v2"
  description = "Platform YCloud"
}

variable "ram" {
  default = 0.5
  description = "Count RAM in GB"
}

variable "cpu" {
  default = 2
  description = ""
}
variable "subnetwork_id" {}

variable "core_fraction" {
  default = 5
}

variable "boot_disk_image_id" {
  default = "fd8t849k1aoosejtcicj"
}
variable "boot_disk_size" {
  default = 5
}

variable "boot_disk_type" {
  default = "network-hdd"
}

variable "boot_disk_block_size" {
  default = 4096
}



