variable "folder_id" {}
variable "cloud_id" {}
variable "token" {}
variable "ram" {
  default = 1
}
variable "zone" {
  default = "ru-central1-a"
}