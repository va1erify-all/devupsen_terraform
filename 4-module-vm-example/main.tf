terraform {
  required_providers {
    yandex = {
      source = "yandex-cloud/yandex"
    }
  }
  required_version = ">= 0.13"
}

provider "yandex" {
  token = var.token
  cloud_id = var.cloud_id
  folder_id = var.folder_id
  zone = var.zone
}

# в данном примере используем хранение модулей локально. Обычно их выносят в registry, например Gitlab infrastructure registry
module "network" {
  source = "./modules/network-create"
  name_network = "main-network"
  name_subnetwork = "main-subnetwork"
}

module "host" {
  count = 1
  ram = var.ram # переопределение переменной модуля
  source = "./modules/vm-create"
  name = "host-${count.index}"
  # отдельные модули друг о друге не знают. Поэтому чтобы обратиться к атрибуту одного модуля из другого, нужно атрибут выносить наружу через "outputs"
  subnetwork_id = module.network.subnetwork_id
}

/*
module "host1" {
  source = "./modules/vm-create"
  name = "host1"
  # отдельные модули друг о друге не знают. Поэтому чтобы обратиться к атрибуту одного модуля из другого, нужно атрибут выносить наружу через "outputs"
  subnetwork_id = module.network.subnetwork_id
}*/
